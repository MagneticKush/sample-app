class StaticPagesController < ApplicationController

  #index main page
  def index
  end
  
  # Home Page Methord with no body
  def home
  end

  # Help Page methord with no body
  def help
  end

  # About Page methord with no body
  def about
  end

  def contact
  	
  end
end
